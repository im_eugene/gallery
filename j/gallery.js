/*
 UTF-8
 im@im-e.ru
 9:16 07.06.2010
 Листалка под собственные нужды.
 */

jQuery.fn.gallery = function(opt){
	var opt = jQuery.extend({
		circle: true,																					// Циклим ли список
		prev: jQuery(this).find('.prev'),																// Пред.
		next: jQuery(this).find('.next'),																// След.
		animationTime: 900,																				// Время анимации
		pauseTime: 1000,																				// Время промежутка между началом след. анимации
		AItem: 0,																						// Индекс текущего эл-та
		mainDrag: true,																					// Листаем ли перетаскиванием главной картинки?
		frameDrag: true,																				// Перетаскиваем ли фрейм?
		countCurrent: jQuery(this).find('.count-current'),												// Контейнер текущего активного элемента
		countAll: jQuery(this).find('.count-all'),														// Контейнер общего количества элементов
		pBlock: jQuery(this).find('.preview'),															// Родительский блок карты-списка
		frame: jQuery(this).find('.frame'),																// Рамка карты
		mainFrame: jQuery(this).find('.main-frame'),													// Блок со списком изображений
		savePosition: true																				// Сохранять активный кадр в строке URL (в виде якоря)
	},opt);
	function parseUrl(){
		var res = new Array(),
			url_split = location.hash.substring(1).split('&');
		for(var i = 0, length = url_split.length; i < length; i++){
			temp = url_split[i].split('=');
			res[temp[0]] = temp[1];
		}
		return res;
	}

	if (opt.savePosition) {
		var url = parseUrl();
	}

	var imgs = opt.mainFrame.find('ul'),																// Список изображений
	diff = 0,																							// При перетаскивании по оси X списка изображений вычисляется расстояние на которое перемещена мышь
		descr = jQuery(this).find('.dcr'),
		mainDrag = false,																				// Клавиша зажата, листаем главное изображение
		lastPos = 0,																					// Положение курсора перед началом листания главного изображения
		moveTo = 0,																						// Элемент, к которому осуществляется движение из крайней позиции.
		focusImg = 0,																					// Элемент, к которому необходимо применить анимацию
		mapAImg = (opt.savePosition && typeof url.slide != 'undefined')?parseInt(url.slide):opt.AItem,	// Смещение (карта)
		lastPos = 0,
		lock = false,																					// Блочить ли действия (если уже происходит анимация, то блочим)
		frame_move = false,																				// Флаг-триггер на движение frame
		pBlockW = opt.pBlock.outerWidth(),																// Ширина родительского блока карты-списка
		map = opt.pBlock.find('ul'),																	// Карта с превьюшками
		mapIW = opt.pBlock.find('ul li').outerWidth(),													// Ширина элемента карты
		totalWidth = 0,																					// Ширина списка изображений
		imgParams = new Array();																		// Массив с параметрами изображений (ширины, отступы)

	opt.frame.append('<div id="cursor"></div>');
	var cursor = opt.frame.find('#cursor');																// Курсор а-ля «e-resize» при перетаскивании фрейма
	cursor.hide();
	jQuery.each(jQuery('li', imgs), function(i, val) {
		totalWidth += (jQuery(val).width());
		imgParams[i] = new Array
		imgParams[i][0] = jQuery(val).width();															// Ширина элемента карты
	});
	var count = imgParams.length;																		// Кол-во элементов в галерее
	opt.countAll.html(count);																			// Общее число элементов
	opt.countCurrent.html(mapAImg+1);																	// Активный элемент
	var mapW = count*mapIW,																				// Ширина карты
	interval = false,																					// Переменная на ф-ю интервала
		timeout = false,
	middle = Math.round(count/2),																		// Середина
	scrolling = (mapW - opt.pBlock.width() > 0) ? true : false;											// Скроллим ли карту
		jQuery.each(jQuery('li', map), function(i, val) {
			jQuery(val).attr('data-position', i);
		});
		map.css('width', mapW+'px');
	var scrollWidth = eval(mapW - pBlockW),																 //«Гуляющая» часть карты

		ofs = ((opt.mainFrame.width() - jQuery('li', imgs).width()) > 0)?eval((opt.mainFrame.width() - jQuery('li', imgs).width())/2):0,
		mapOffset = eval( (scrollWidth) / (pBlockW - opt.frame.width()) );



	jQuery.each(jQuery('li', imgs), function(i) {
		if (scrolling) {
			imgParams[i][2] = (scrollWidth / count * i * 1.3);
			if (imgParams[i][2] > scrollWidth) {														// Отступ карты
				imgParams[i][2] = (count * mapIW - pBlockW + 3);
			}
			imgParams[i][1] = (mapIW * i - imgParams[i][2]);											// Отступ frame при активности текущего элемента
		} else {
			imgParams[i][1] = i*mapIW;
			imgParams[i][2] = 0;
		}
		sum = 0;
		for (y = 0; y < i; y++) {
			sum += parseInt(imgParams[y][0], 10);
		}
		imgParams[i][3] = sum - ofs;
	});



	if (count > 3) {
		for (i = 1; i <= imgParams.length; i++) {
			jQuery('li:nth-child(' + i + ')', imgs).clone().insertAfter(jQuery('li:last', imgs));
		}
		for (pos = imgParams.length; pos >= 1; pos--) {
			jQuery('li:nth-child(' + count + ')', imgs).clone().insertBefore(jQuery('li:nth-child(1)', imgs));
		}
//		imgs.width((totalWidth * 3) + 'px');
	}



	jQuery('li:nth-child(' + (mapAImg + 1) + ') img', map).addClass('act-image');								// Задание класса для активного изображения
	mFrame();
	mapScroll();
	direct(0, mapAImg, (imgParams[mapAImg][3] + totalWidth));
	itemMove();
	check_btns();

	// Проверка: блочить ли кнопки?
	function check_btns(){
		if (count <= 3 && mapAImg <= 0) {
			opt.prev.addClass('disabled');
		} else if (count >= 3) {
			opt.prev.removeClass('disabled');
		}
		if (count <= 3 && mapAImg >= (count-1)) {
			opt.next.addClass('disabled');
		} else if (count >= 3) {
			opt.next.removeClass('disabled');
		}
		if (!opt.circle) {
			if (mapAImg == 0) {
				opt.prev.addClass('disabled');
			}
			if (mapAImg == count - 1) {
				opt.next.addClass('disabled');
			}
		}
	}



	// Перетаскивание мышкой главного изображения по оси X
	if (opt.mainDrag && count > 3) {
		opt.mainFrame.css('cursor', 'e-resize');
		imgs.mousedown( function(e){
			if (!lock) {
				mainDrag = true;
				diff = 0;
				lPos = e.pageX;																				// Положение курсора при начале перетаскивания
				currPos = totalWidth + imgParams[mapAImg][3];												// Положение списка изображений при начале перетаскивания
			}
			return false;
		}).mousemove( function(e){
				if (mainDrag && !lock) {
					diff = eval(e.pageX - lPos);
					imgs.css('left', '-' + (currPos - diff) + 'px');
				}
				return false;
			}).mouseup( function(){
				mainDrag = false;
				if (Math.abs(diff) > 99) {
					focusImg = (diff > 0) ? (mapAImg - 1) : (mapAImg + 1);
					if (focusImg < 0) {
						focusImg = (count+focusImg);
					}
					if (focusImg > (count-1)) {
						focusImg = (focusImg-count);
					}
					direct(mapAImg, focusImg, diff);
					mapAImg = focusImg;
					itemMove();
					focusImg = 0;
					mFrame();
					mapScroll();
				} else {
					imgs.css('left', '-'+imgParams[mapAImg][3]+'px');
				}
				return false;
			});
	}



	console.log(opt.frameDrag);
	if (opt.frameDrag) {
		// Вычисление отступа фрейма, проверка: не выходим ли мы за границы видимой части карты
		function frameOffset(x){
			coord = x - opt.frame.width() / 2;
			if(coord >= (pBlockW-opt.frame.width() + 1)){
				coord = (pBlockW-opt.frame.width() + 1);
			} else if (coord <= 0) {
				coord = 0;
			}
			return coord;
		}
		// Перетаскивание фрейма по карте
		opt.frame.mousedown( function() {
			if (! lock) {
				cursor.show();
				frame_move = true;
				focusImg = mapAImg;
				jQuery('li img', map).removeClass('act-image');
			}
			return false;
		}).mousemove( function(e){
				if (frame_move && count > 3) {
					keyPressFrameMove(e);
				}
				return false;
			}).mouseout( function() {
				if (count > 3) {
					jQuery('body').mousemove( function(e){
						if(frame_move && count > 3){
							keyPressFrameMove(e);
						}
					});
					jQuery('body').mouseup( function() {
						if (frame_move) {
							mouseup();
						}
					});
				}
				return false;
			}).mouseup( function() {
				mouseup();
				return false;
			});



		// Двигаем фрейм мышкой и прокручиваем карту
		function keyPressFrameMove(e){
			//			cursor.show().css({top: (e.pageY-10)+'px', left: (e.pageX-5)+'px'});
			// Вычисление отступа фрейма относительно видимой части карты
			var x = (e.pageX - opt.pBlock.offset().left);
			fPos = frameOffset(x);
			opt.frame.css('left', fPos + 'px');
			if (scrolling) {
				// Коэффицент смещения умножаем на положение фрейма — получаем отступ, на который нужно сместить карту
				diffMove = mapOffset * fPos;
				map.css('left', '-'+diffMove+'px');
			} else {
				diffMove = 0;
			}
			focusImg = parseInt((fPos + diffMove + (mapIW/2)) / mapIW, 10);
			lastPos = focusImg;
			return false;
		}

		function mouseup() {
			cursor.hide();
			if (mapAImg != focusImg) {
				direct(mapAImg, focusImg);
				mapAImg = focusImg;
				itemMove();
				focusImg = 0;
			}
			mFrame();
			mapScroll();
			frame_move = false;
		}
	}

	// Пользователь перестал таскать фрейм, выделяем активный элемент на карте
	function mFrame() {
		jQuery('li img', map).removeClass('act-image');
		opt.frame.animate({ left: imgParams[mapAImg][1] + 'px' }, opt.animationTime, function() {
			jQuery('li:nth-child(' + (mapAImg + 1) + ') img', map).addClass('act-image');						// Перемещение окошка карты
			opt.countCurrent.html(mapAImg + 1);
		} );
	}

	// Прокручивание карты
	function mapScroll() {
		if(scrolling){
			jQuery(map).animate({ left: '-' + imgParams[mapAImg][2] + 'px' }, opt.animationTime);
		}
	}



	jQuery('html').mouseup( function() {
		if (interval) {
			window.clearInterval(interval);
		}
	});

	opt.prev.click( function() { // Клик «Назад»
		if (! lock && ! opt.prev.hasClass('disabled')) {
			movieToLeft();
		}
	}).mousedown( function() { // Кнопка зажата, двигаемся
			if (! opt.prev.hasClass('disabled')) {
				interval = window.setInterval(function() {
					if ((mapAImg != 0 && count <= 3) || scrolling) {
						movieToLeft();
					}
				}, opt.pauseTime);
			}
		});
	function movieToLeft() {
		if (mapAImg == 0) {
			focusImg = (count - 1);
			direct(mapAImg, focusImg);
			mapAImg = focusImg;
		} else {
			focusImg = mapAImg-1;
			direct(mapAImg, focusImg);
			mapAImg -= 1;
		}
		itemMove();
		mFrame();
		mapScroll();
	}

	opt.next.click( function() { // Клик «Вперёд»
		if (! lock && ! opt.next.hasClass('disabled')) {
			movieToRight();
		}
	}).mousedown( function() { // Кнопка зажата, двигаемся
			if (! opt.next.hasClass('disabled')) {
				interval = window.setInterval(function() {
					if ((mapAImg != (count-1) && count <= 3) || scrolling) {
						movieToRight();
					}
				}, opt.pauseTime);
			}
		});
	function movieToRight() {
		if (mapAImg == (count-1)) {
			focusImg = 0;
			direct(mapAImg, focusImg);
			mapAImg = focusImg;
		} else {
			focusImg = mapAImg + 1;
			direct(mapAImg, focusImg);
			mapAImg += 1;
		}
		itemMove();
		mFrame();
		mapScroll();
	}

	jQuery('li', map).click( function() {
		if (! lock) {
			if (count > 3) {
				direct(parseInt(mapAImg), parseInt(jQuery(this).attr('data-position')));
			} else {
				moveTo = imgParams[parseInt(jQuery(this).attr('data-position'))][3];
			}
			mapAImg = parseInt(jQuery(this).attr('data-position'));
			itemMove();
			mFrame();
			mapScroll();
		}
		return false;
	});



	function itemMove() {
		opt.countCurrent.html(mapAImg + 1);
		opt.prev.addClass('disabled');
		opt.next.addClass('disabled');
		lock = true;
		opt.AItem = mapAImg;
		descr.html(jQuery('li:nth-child(' + (mapAImg + 1) + ') div', imgs).html());
		jQuery(imgs).animate({ left: '-' + moveTo + 'px' }, opt.animationTime, function() {
			check_btns();
			lock = false;
			if (timeout) {
				clearTimeout(timeout);
				timeout = false;
			}
		});
		if (opt.savePosition) {
			window.location.href = '#slide=' + mapAImg;
		}
	}



	// Функция в которой назначаются исходная позиция и позиция на которой анимация заканчивается
	function direct(lastPos, nextPos, currPos){
		if (typeof currPos === 'undefined') {
			currPos = 0;
		}
		if (opt.circle) {
			// Если листалка зациклена, то выставляем необходимые позиции: позиция до начала анимации и позиция анимации
			if (Math.abs(lastPos - nextPos) > middle && lastPos < middle) {
				// Анимация прокрутки по кратчайшему пути: справа-налево. Начальная позиция выставляется в правой копии списка, конечная — в центральной
				imgs.css('left', '-' + Math.abs(totalWidth + imgParams[lastPos][3] - parseInt(currPos)) + 'px');
				moveTo = imgParams[nextPos][3];
			} else if (Math.abs(lastPos - nextPos) > middle && lastPos > middle) {
				// Анимация прокрутки по кратчайшему пути: сналева—направо. Начальная позиция выставляется в левой копии списка, конечная — в центральной
				imgs.css('left', '-' + Math.abs(eval(imgParams[lastPos][3] - parseInt(currPos))) + 'px');
				moveTo = imgParams[nextPos][3] + totalWidth;
			} else {
				// Простая анимация внутри центральной копии списка
				imgs.css('left', '-' + Math.abs(imgParams[lastPos][3] - parseInt(currPos)) + 'px');
				moveTo = imgParams[nextPos][3];
			}
		} else {
			// Простая анимация внутри центральной копии списка
			imgs.css('left', '-' + imgParams[lastPos][3] + 'px');
			moveTo = imgParams[nextPos][3];
		}
	}
};